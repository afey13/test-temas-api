package usecaseimpl

import (
	"context"

	"gitlab.com/aerilyn/service-library/config"
	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/afey13/test-temas-api/internal/app/character/repository"
	"gitlab.com/afey13/test-temas-api/internal/app/character/usecase"
	"gitlab.com/afey13/test-temas-api/internal/app/ent"
)

type ListOptions struct {
	Config              config.ConfigEnv
	CharacterRepository repository.CharacterRepository
}

type List struct {
	options ListOptions
}

func NewList(options ListOptions) *List {
	return &List{
		options: options,
	}
}

func (s *List) List(ctx context.Context, cmd usecase.ListCommand) ([]ent.Character, errors.CodedError) {
	characters, err := s.options.CharacterRepository.List(ctx, int(cmd.Page), int(cmd.Limit))
	if err != nil {
		return nil, err
	}
	return characters, nil
}
