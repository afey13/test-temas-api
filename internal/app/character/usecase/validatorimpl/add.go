package validatorimpl

import (
	"context"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/aerilyn/service-library/validation/ozzomapper"
	"gitlab.com/afey13/test-temas-api/internal/app/character/usecase"
)

type AddValidatorOptions struct {
}

type AddValidator struct {
	Options AddValidatorOptions
}

func NewAddValidator(opts AddValidatorOptions) *AddValidator {
	return &AddValidator{
		Options: opts,
	}
}

func (s AddValidator) Validate(ctx context.Context, cmd usecase.AddCommand) errors.CodedError {
	validationErr := validation.ValidateStruct(
		&cmd,
		validation.Field(&cmd.Name, validation.Required),
		validation.Field(&cmd.Class, validation.Required),
	)
	switch e := validationErr.(type) {
	case validation.InternalError:
		return errors.NewInternalSystemError().CopyWith(errors.Message(e.Error()))
	case validation.Errors:
		invalidParameterErr := errors.NewInvalidParameterError()
		return ozzomapper.WrapValidationError(invalidParameterErr.Code(), invalidParameterErr.MessageKey(), invalidParameterErr.Message(), e)
	}
	return nil
}
