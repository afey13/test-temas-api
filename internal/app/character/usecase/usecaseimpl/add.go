package usecaseimpl

import (
	"context"

	"gitlab.com/aerilyn/service-library/config"
	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/afey13/test-temas-api/internal/app/character/repository"
	"gitlab.com/afey13/test-temas-api/internal/app/character/usecase"
	"gitlab.com/afey13/test-temas-api/internal/app/ent"
)

type AddOptions struct {
	Config              config.ConfigEnv
	CharacterRepository repository.CharacterRepository
	AddValidator        usecase.AddValidator
}

type Add struct {
	options AddOptions
}

func NewAdd(options AddOptions) *Add {
	return &Add{
		options: options,
	}
}

func (s *Add) Add(ctx context.Context, cmd usecase.AddCommand) (*usecase.AddDTO, errors.CodedError) {
	err := s.options.AddValidator.Validate(ctx, cmd)
	if err != nil {
		return nil, err
	}
	character := ent.Character{
		Name:         cmd.Name,
		HitPoints:    100,
		Defense:      10,
		Intelligence: 10,
		Strength:     10,
		Class:        cmd.Class,
	}
	char, err := s.options.CharacterRepository.Add(ctx, character)
	if err != nil {
		return nil, err
	}
	dto := usecase.AddDTO{
		ID: char.ID,
	}
	return &dto, nil
}
