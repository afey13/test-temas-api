package mysql

import (
	"fmt"
	"sync"
	"time"

	"gitlab.com/aerilyn/service-library/config"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var (
	err       error
	db        *gorm.DB
	onceMySQL sync.Once
)

func ProvideClient(cfg *config.Config) *gorm.DB {
	onceMySQL.Do(func() {
		dsn := cfg.Get(config.MYSQL_URL)
		fmt.Println("connect mysql with connection => ", dsn)
		db, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})
		if err != nil {
			panic(err)
		}
		setting, err := db.DB()
		if err != nil {
			panic(err)
		}
		setting.SetConnMaxIdleTime(10)
		setting.SetMaxOpenConns(10)
		setting.SetConnMaxLifetime(time.Minute * 10)
	})
	return db
}
