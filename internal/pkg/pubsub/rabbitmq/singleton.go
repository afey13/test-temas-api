package rabbitmq

//go:generate mockgen -source=singleton.go -destination=mock/singleton_mock.go -package=mock

import (
	"context"
	"sync"

	"github.com/ThreeDotsLabs/watermill"
	"github.com/ThreeDotsLabs/watermill-amqp/pkg/amqp"
	"github.com/ThreeDotsLabs/watermill/message"
)

var (
	mqSubscriber      Subscriber
	mqPublisher       Publisher
	onceAMPQSubcribe  sync.Once
	onceAMPQPublisher sync.Once
)

type Subscriber interface {
	Subscribe(ctx context.Context, topic string) (<-chan *message.Message, error)
	Close() error
}

type Publisher interface {
	Publish(topic string, messages ...*message.Message) error
	Close() error
}

func NewProviderSubscriberWithAMQP(loggerAdapter watermill.LoggerAdapter, amqpConfig amqp.Config) Subscriber {
	onceAMPQSubcribe.Do(func() {
		var err error

		mqSubscriber, err = amqp.NewSubscriber(
			amqpConfig,
			loggerAdapter,
		)
		if err != nil {
			panic(err)
		}

	})
	return mqSubscriber

}

func NewProviderPublihserWithAMQP(loggerAdapter watermill.LoggerAdapter, amqpConfig amqp.Config) Publisher {
	onceAMPQPublisher.Do(func() {
		var err error

		mqPublisher, err = amqp.NewPublisher(
			amqpConfig,
			loggerAdapter,
		)
		if err != nil {
			panic(err)
		}

	})
	return mqPublisher

}
