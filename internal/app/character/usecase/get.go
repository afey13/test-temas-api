package usecase

//go:generate mockgen -source=get.go -destination=usecasemock/get_mock.go -package=usecasemock

import (
	"context"

	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/afey13/test-temas-api/internal/app/ent"
)

type GetCommand struct {
	ID int `json:"id"`
}

type Get interface {
	Get(ctx context.Context, ID int) (*ent.Character, errors.CodedError)
}
