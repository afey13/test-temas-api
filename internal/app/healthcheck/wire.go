package healthcheck

import (
	"gitlab.com/afey13/test-temas-api/internal/app/healthcheck/delivery/web"

	"github.com/google/wire"
)

var ModuleSet = wire.NewSet(
	web.NewHandlerRegistry,
)
