// Code generated by MockGen. DO NOT EDIT.
// Source: character.go

// Package repositorymock is a generated GoMock package.
package repositorymock

import (
	context "context"
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
	errors "gitlab.com/aerilyn/service-library/errors"
	ent "gitlab.com/afey13/test-temas-api/internal/app/ent"
)

// MockCharacterRepository is a mock of CharacterRepository interface.
type MockCharacterRepository struct {
	ctrl     *gomock.Controller
	recorder *MockCharacterRepositoryMockRecorder
}

// MockCharacterRepositoryMockRecorder is the mock recorder for MockCharacterRepository.
type MockCharacterRepositoryMockRecorder struct {
	mock *MockCharacterRepository
}

// NewMockCharacterRepository creates a new mock instance.
func NewMockCharacterRepository(ctrl *gomock.Controller) *MockCharacterRepository {
	mock := &MockCharacterRepository{ctrl: ctrl}
	mock.recorder = &MockCharacterRepositoryMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockCharacterRepository) EXPECT() *MockCharacterRepositoryMockRecorder {
	return m.recorder
}

// Add mocks base method.
func (m *MockCharacterRepository) Add(ctx context.Context, character ent.Character) (*ent.Character, errors.CodedError) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Add", ctx, character)
	ret0, _ := ret[0].(*ent.Character)
	ret1, _ := ret[1].(errors.CodedError)
	return ret0, ret1
}

// Add indicates an expected call of Add.
func (mr *MockCharacterRepositoryMockRecorder) Add(ctx, character interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Add", reflect.TypeOf((*MockCharacterRepository)(nil).Add), ctx, character)
}

// Get mocks base method.
func (m *MockCharacterRepository) Get(ctx context.Context, ID int) (*ent.Character, errors.CodedError) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Get", ctx, ID)
	ret0, _ := ret[0].(*ent.Character)
	ret1, _ := ret[1].(errors.CodedError)
	return ret0, ret1
}

// Get indicates an expected call of Get.
func (mr *MockCharacterRepositoryMockRecorder) Get(ctx, ID interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Get", reflect.TypeOf((*MockCharacterRepository)(nil).Get), ctx, ID)
}

// List mocks base method.
func (m *MockCharacterRepository) List(ctx context.Context, page, limit int) ([]ent.Character, errors.CodedError) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "List", ctx, page, limit)
	ret0, _ := ret[0].([]ent.Character)
	ret1, _ := ret[1].(errors.CodedError)
	return ret0, ret1
}

// List indicates an expected call of List.
func (mr *MockCharacterRepositoryMockRecorder) List(ctx, page, limit interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "List", reflect.TypeOf((*MockCharacterRepository)(nil).List), ctx, page, limit)
}
