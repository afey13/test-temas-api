package usecase

//go:generate mockgen -source=list.go -destination=usecasemock/list_mock.go -package=usecasemock

import (
	"context"

	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/afey13/test-temas-api/internal/app/ent"
)

type ListCommand struct {
	Page  int64
	Limit int64
}

type List interface {
	List(ctx context.Context, cmd ListCommand) ([]ent.Character, errors.CodedError)
}
