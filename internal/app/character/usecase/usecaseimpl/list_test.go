package usecaseimpl

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	. "github.com/smartystreets/goconvey/convey"
	configMock "gitlab.com/aerilyn/service-library/config/mock"
	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/afey13/test-temas-api/internal/app/character/repository/repositorymock"
	"gitlab.com/afey13/test-temas-api/internal/app/character/usecase"
	"gitlab.com/afey13/test-temas-api/internal/app/ent"
)

func TestCharacterList(t *testing.T) {
	ctx := context.Background()
	cmd := usecase.ListCommand{
		Page:  1,
		Limit: 1,
	}
	entCharacter := ent.Character{
		Name:         "Arjuna",
		HitPoints:    100,
		Defense:      10,
		Intelligence: 10,
		Strength:     10,
		Class:        1,
	}
	resultError := []ent.Character{}
	result := []ent.Character{
		entCharacter,
		entCharacter,
		entCharacter,
	}
	Convey("Testing Get Character By ID", t, func() {
		mockCtrl := gomock.NewController(t)
		Reset(func() {
			mockCtrl.Finish()
		})
		configMock := configMock.NewMockConfigEnv(mockCtrl)
		characterRepositoryMock := repositorymock.NewMockCharacterRepository(mockCtrl)
		listOptions := ListOptions{
			Config:              configMock,
			CharacterRepository: characterRepositoryMock,
		}
		uc := NewList(listOptions)
		Convey("when get data error should return error internal system error", func() {
			characterRepositoryMock.EXPECT().List(ctx, int(cmd.Page), int(cmd.Limit)).Return(resultError, errors.NewInternalSystemError())
			res, errCode := uc.List(ctx, cmd)
			So(errCode, ShouldNotBeNil)
			So(errCode.Code(), ShouldEqual, errors.NewInternalSystemError().Code())
			So(res, ShouldBeNil)
		})
		Convey("when all process not return error should not return error nil", func() {
			characterRepositoryMock.EXPECT().List(ctx, int(cmd.Page), int(cmd.Limit)).Return(result, nil)
			res, errCode := uc.List(ctx, cmd)
			So(errCode, ShouldBeNil)
			So(res, ShouldNotBeNil)
			So(res, ShouldResemble, result)
		})
	})
}
