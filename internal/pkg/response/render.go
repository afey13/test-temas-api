package response

import (
	"net/http"

	"github.com/go-chi/render"
	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/aerilyn/service-library/log"
)

type ResponseResult struct {
	Data    interface{} `json:"data"`
	Success bool        `json:"success"`
	Message *string     `json:"message"`
}

var presetErrorToHttpStatusMapping = map[int]int{
	errors.NewInvalidParameterError().Code():               http.StatusBadRequest,
	errors.NewInternalSystemError().Code():                 http.StatusInternalServerError,
	errors.NewEntityNotFound().Code():                      http.StatusNotFound,
	errors.NewForbiddenAccessError().Code():                http.StatusForbidden,
	errors.NewAuthorizationError().Code():                  http.StatusUnauthorized,
	errors.NewIdempotentTransactionViolationError().Code(): http.StatusConflict,
}

func Render(opts log.ResponseOpts) {
	var message *string
	success := true
	message = nil
	statusCode := opts.StatusCode
	if opts.Err != nil {
		statusCode, message = checkError(opts.Err)
		success = false
	}
	result := ResponseResult{
		Data:    opts.Result,
		Success: success,
		Message: message,
	}
	log.WithCTX(opts.Ctx).PrintResponse(opts)
	render.Status(opts.Request, statusCode)
	render.JSON(opts.Writer, opts.Request, result)
}

func checkError(err errors.CodedError) (int, *string) {
	message := err.Message()
	code := presetErrorToHttpStatusMapping[err.Code()]
	if code != 0 {
		return code, &message
	}
	switch err.(type) {
	case errors.CodedFieldsError:
		return http.StatusBadRequest, &message
	default:
		return http.StatusInternalServerError, &message
	}
}
