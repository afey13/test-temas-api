package web

import (
	"gitlab.com/afey13/test-temas-api/internal/app/character/usecase"

	"github.com/go-chi/chi"
)

type CharacterRegistryOptions struct {
	List usecase.List
	Add  usecase.Add
	Get  usecase.Get
}

type CharacterHandlerRegistry struct {
	options CharacterRegistryOptions
}

func NewCharacterHandlerRegistry(options CharacterRegistryOptions) *CharacterHandlerRegistry {
	return &CharacterHandlerRegistry{
		options: options,
	}
}

func (h *CharacterHandlerRegistry) RegisterRoutesTo(r chi.Router) error {
	r.Route("/characters", func(r chi.Router) {
		r.Get("/", ListWebHandler(h.options.List))
	})
	r.Route("/character", func(r chi.Router) {
		r.Post("/", AddWebHandler(h.options.Add))
		r.Get("/{id}/", GetWebHandler(h.options.Get))
	})
	return nil
}
