// Code generated by MockGen. DO NOT EDIT.
// Source: list.go

// Package usecasemock is a generated GoMock package.
package usecasemock

import (
	context "context"
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
	errors "gitlab.com/aerilyn/service-library/errors"
	usecase "gitlab.com/afey13/test-temas-api/internal/app/character/usecase"
	ent "gitlab.com/afey13/test-temas-api/internal/app/ent"
)

// MockList is a mock of List interface.
type MockList struct {
	ctrl     *gomock.Controller
	recorder *MockListMockRecorder
}

// MockListMockRecorder is the mock recorder for MockList.
type MockListMockRecorder struct {
	mock *MockList
}

// NewMockList creates a new mock instance.
func NewMockList(ctrl *gomock.Controller) *MockList {
	mock := &MockList{ctrl: ctrl}
	mock.recorder = &MockListMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockList) EXPECT() *MockListMockRecorder {
	return m.recorder
}

// List mocks base method.
func (m *MockList) List(ctx context.Context, cmd usecase.ListCommand) ([]ent.Character, errors.CodedError) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "List", ctx, cmd)
	ret0, _ := ret[0].([]ent.Character)
	ret1, _ := ret[1].(errors.CodedError)
	return ret0, ret1
}

// List indicates an expected call of List.
func (mr *MockListMockRecorder) List(ctx, cmd interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "List", reflect.TypeOf((*MockList)(nil).List), ctx, cmd)
}
