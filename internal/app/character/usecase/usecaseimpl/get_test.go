package usecaseimpl

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	. "github.com/smartystreets/goconvey/convey"
	configMock "gitlab.com/aerilyn/service-library/config/mock"
	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/afey13/test-temas-api/internal/app/character/repository/repositorymock"
	"gitlab.com/afey13/test-temas-api/internal/app/ent"
)

func TestCharacterGet(t *testing.T) {
	ctx := context.Background()
	ID := 1

	entCharacterResult := ent.Character{
		ID:           ID,
		Name:         "Arjuna",
		HitPoints:    100,
		Defense:      10,
		Intelligence: 10,
		Strength:     10,
		Class:        1,
	}

	Convey("Testing Get Character By ID", t, func() {
		mockCtrl := gomock.NewController(t)
		Reset(func() {
			mockCtrl.Finish()
		})
		configMock := configMock.NewMockConfigEnv(mockCtrl)
		characterRepositoryMock := repositorymock.NewMockCharacterRepository(mockCtrl)
		getOptions := GetOptions{
			Config:              configMock,
			CharacterRepository: characterRepositoryMock,
		}
		uc := NewGet(getOptions)
		Convey("when get data but not found data should return error entity not found", func() {
			characterRepositoryMock.EXPECT().Get(ctx, ID).Return(nil, errors.NewEntityNotFound())
			res, errCode := uc.Get(ctx, ID)
			So(errCode, ShouldNotBeNil)
			So(errCode.Code(), ShouldEqual, errors.NewEntityNotFound().Code())
			So(res, ShouldBeNil)
		})
		Convey("when get data error should return error internal system error", func() {
			characterRepositoryMock.EXPECT().Get(ctx, ID).Return(nil, errors.NewInternalSystemError())
			res, errCode := uc.Get(ctx, ID)
			So(errCode, ShouldNotBeNil)
			So(errCode.Code(), ShouldEqual, errors.NewInternalSystemError().Code())
			So(res, ShouldBeNil)
		})
		Convey("when all process not return error should not return error nil", func() {
			characterRepositoryMock.EXPECT().Get(ctx, ID).Return(&entCharacterResult, nil)
			res, errCode := uc.Get(ctx, ID)
			So(errCode, ShouldBeNil)
			So(res, ShouldNotBeNil)
			So(res, ShouldResemble, &entCharacterResult)
		})
	})
}
