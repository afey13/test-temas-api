package example

import (
	"gitlab.com/afey13/test-temas-api/internal/app/character/delivery/web"
	"gitlab.com/afey13/test-temas-api/internal/app/character/repository"
	"gitlab.com/afey13/test-temas-api/internal/app/character/repository/repositorymysql"
	"gitlab.com/afey13/test-temas-api/internal/app/character/usecase"
	"gitlab.com/afey13/test-temas-api/internal/app/character/usecase/usecaseimpl"
	"gitlab.com/afey13/test-temas-api/internal/app/character/usecase/validatorimpl"

	"github.com/google/wire"
)

var ModuleSet = wire.NewSet(
	repositoryModuleSet,
	validatorModuleSet,
	usecaseModuleSet,
	deliveryModuleSet,
)

var repositoryModuleSet = wire.NewSet(
	wire.Struct(new(repositorymysql.MySQLCharacterRepositoryOptions), "*"),
	repositorymysql.NewMySQLCharacterRepository,
	wire.Bind(new(repository.CharacterRepository), new(*repositorymysql.MySQLCharacterRepository)),
)

var deliveryModuleSet = wire.NewSet(
	wire.Struct(new(web.CharacterRegistryOptions), "*"),
	web.NewCharacterHandlerRegistry,
)

var validatorModuleSet = wire.NewSet(
	wire.Struct(new(validatorimpl.AddValidatorOptions), "*"),
	validatorimpl.NewAddValidator,
	wire.Bind(new(usecase.AddValidator), new(*validatorimpl.AddValidator)),
)

var usecaseModuleSet = wire.NewSet(
	wire.Struct(new(usecaseimpl.ListOptions), "*"),
	usecaseimpl.NewList,
	wire.Bind(new(usecase.List), new(*usecaseimpl.List)),
	wire.Struct(new(usecaseimpl.AddOptions), "*"),
	usecaseimpl.NewAdd,
	wire.Bind(new(usecase.Add), new(*usecaseimpl.Add)),
	wire.Struct(new(usecaseimpl.GetOptions), "*"),
	usecaseimpl.NewGet,
	wire.Bind(new(usecase.Get), new(*usecaseimpl.Get)),
)
