package repository

//go:generate mockgen -source=character.go -destination=repositorymock/character_mock.go -package=repositorymock

import (
	"context"

	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/afey13/test-temas-api/internal/app/ent"
)

type FindOptions struct {
	Limit  int64
	Offset int64
	// Filter bson.M
}

type CharacterRepository interface {
	List(ctx context.Context, page, limit int) ([]ent.Character, errors.CodedError)
	Add(ctx context.Context, character ent.Character) (*ent.Character, errors.CodedError)
	Get(ctx context.Context, ID int) (*ent.Character, errors.CodedError)
}
