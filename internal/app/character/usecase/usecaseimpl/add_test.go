package usecaseimpl

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	. "github.com/smartystreets/goconvey/convey"
	configMock "gitlab.com/aerilyn/service-library/config/mock"
	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/afey13/test-temas-api/internal/app/character/repository/repositorymock"
	"gitlab.com/afey13/test-temas-api/internal/app/character/usecase"
	"gitlab.com/afey13/test-temas-api/internal/app/character/usecase/usecasemock"
	"gitlab.com/afey13/test-temas-api/internal/app/ent"
)

func TestCharacterAdd(t *testing.T) {
	ctx := context.Background()
	addCommand := usecase.AddCommand{
		Name:  "Arjuna",
		Class: 10,
	}

	entCharacter := ent.Character{
		Name:         addCommand.Name,
		HitPoints:    100,
		Defense:      10,
		Intelligence: 10,
		Strength:     10,
		Class:        addCommand.Class,
	}

	entCharacterResult := ent.Character{
		ID:           1,
		Name:         addCommand.Name,
		HitPoints:    100,
		Defense:      10,
		Intelligence: 10,
		Strength:     10,
		Class:        addCommand.Class,
	}

	dto := usecase.AddDTO{
		ID: entCharacterResult.ID,
	}
	Convey("Testing Add Character", t, func() {
		mockCtrl := gomock.NewController(t)
		Reset(func() {
			mockCtrl.Finish()
		})
		configMock := configMock.NewMockConfigEnv(mockCtrl)
		characterRepositoryMock := repositorymock.NewMockCharacterRepository(mockCtrl)
		characaterAddValidatorMock := usecasemock.NewMockAddValidator(mockCtrl)
		addOptions := AddOptions{
			Config:              configMock,
			CharacterRepository: characterRepositoryMock,
			AddValidator:        characaterAddValidatorMock,
		}
		uc := NewAdd(addOptions)
		Convey("when validate return error should return error invalid parameter", func() {
			characaterAddValidatorMock.EXPECT().Validate(ctx, addCommand).Return(errors.NewInvalidParameterError())
			res, errCode := uc.Add(ctx, addCommand)
			So(errCode, ShouldNotBeNil)
			So(errCode.Code(), ShouldEqual, errors.NewInvalidParameterError().Code())
			So(res, ShouldBeNil)
		})
		Convey("when add data error should return error internal system error", func() {
			characaterAddValidatorMock.EXPECT().Validate(ctx, addCommand).Return(nil)
			characterRepositoryMock.EXPECT().Add(ctx, entCharacter).Return(nil, errors.NewInternalSystemError())
			res, errCode := uc.Add(ctx, addCommand)
			So(errCode, ShouldNotBeNil)
			So(errCode.Code(), ShouldEqual, errors.NewInternalSystemError().Code())
			So(res, ShouldBeNil)
		})
		Convey("when all process not return error should not return error nil", func() {
			characaterAddValidatorMock.EXPECT().Validate(ctx, addCommand).Return(nil)
			characterRepositoryMock.EXPECT().Add(ctx, entCharacter).Return(&entCharacterResult, nil)
			res, errCode := uc.Add(ctx, addCommand)
			So(errCode, ShouldBeNil)
			So(res, ShouldNotBeNil)
			So(res, ShouldResemble, &dto)
		})
	})
}
