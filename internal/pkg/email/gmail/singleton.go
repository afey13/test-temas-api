package gmail

//go:generate mockgen -source=singleton.go -destination=mock/singleton_mock.go -package=mock

import (
	"strconv"
	"sync"

	"gitlab.com/aerilyn/service-library/config"
	"gopkg.in/gomail.v2"
)

var (
	gmail     *gomail.Dialer
	onceGmail sync.Once
)

type GomailDialer interface {
	DialAndSend(m ...*gomail.Message) error
}

func ProviderGmail(cfg config.Config) *gomail.Dialer {
	onceGmail.Do(func() {
		port, _ := strconv.Atoi(cfg.Get(config.GMAIL_PORT))
		gmail = gomail.NewDialer(
			cfg.Get(config.GMAIL_HOST),
			port,
			cfg.Get(config.GMAIL_USERNAME),
			cfg.Get(config.GMAIL_PASSWORD),
		)
	})
	return gmail

}
