package usecase

//go:generate mockgen -source=add.go -destination=usecasemock/add_mock.go -package=usecasemock

import (
	"context"

	"gitlab.com/aerilyn/service-library/errors"
)

type AddCommand struct {
	Name  string `json:"name"`
	Class int    `json:"class"`
}

type AddDTO struct {
	ID int `json:"id"`
}

type Add interface {
	Add(ctx context.Context, cmd AddCommand) (*AddDTO, errors.CodedError)
}
