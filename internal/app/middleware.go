package app

import (
	"gitlab.com/aerilyn/service-library/middleware/log"
)

type RequiredMiddlewares struct {
	Logging *log.LoggingMiddlewareRegistry
}
