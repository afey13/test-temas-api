package usecaseimpl

import (
	"context"

	"gitlab.com/aerilyn/service-library/config"
	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/afey13/test-temas-api/internal/app/character/repository"
	"gitlab.com/afey13/test-temas-api/internal/app/ent"
)

type GetOptions struct {
	Config              config.ConfigEnv
	CharacterRepository repository.CharacterRepository
}

type Get struct {
	options GetOptions
}

func NewGet(options GetOptions) *Get {
	return &Get{
		options: options,
	}
}

func (s *Get) Get(ctx context.Context, ID int) (*ent.Character, errors.CodedError) {
	character, err := s.options.CharacterRepository.Get(ctx, ID)
	if err != nil {
		return nil, err
	}
	return character, nil
}
