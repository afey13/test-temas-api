package app

import (
	characterweb "gitlab.com/afey13/test-temas-api/internal/app/character/delivery/web"
	healthcheckweb "gitlab.com/afey13/test-temas-api/internal/app/healthcheck/delivery/web"
)

// jhipster-needle-import-handler

type RequiredHandlers struct {
	HealthCheckHTTPHandlerRegistry *healthcheckweb.HealthCheckHandlerRegistry
	ExampleHTTPHandlerRegistry     *characterweb.CharacterHandlerRegistry
}
