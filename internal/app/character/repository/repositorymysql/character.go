package repositorymysql

import (
	"context"

	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/afey13/test-temas-api/internal/app/ent"
	"gorm.io/gorm"
)

type MySQLCharacterRepositoryOptions struct {
	Db *gorm.DB
}

type MySQLCharacterRepository struct {
	options MySQLCharacterRepositoryOptions
}

func NewMySQLCharacterRepository(options MySQLCharacterRepositoryOptions) *MySQLCharacterRepository {
	return &MySQLCharacterRepository{options: options}
}

func (m *MySQLCharacterRepository) collection() *gorm.DB {
	return m.options.Db.Table(TblCharacter)
}

func (m *MySQLCharacterRepository) List(ctx context.Context, page, limit int) ([]ent.Character, errors.CodedError) {
	offset := (page - 1) * limit
	characters := []ent.Character{}
	result := m.collection().Offset(offset).Limit(limit).Find(&characters)
	if result.Error != nil {
		return nil, errors.NewInternalSystemError().CopyWith(errors.Message("error when get data"))
	}

	return characters, nil
}

func (m *MySQLCharacterRepository) Add(ctx context.Context, character ent.Character) (*ent.Character, errors.CodedError) {
	result := m.collection().Create(&character)
	if result.Error != nil {
		return nil, errors.NewInternalSystemError().CopyWith(errors.Message("error when insert data"))
	}

	return &character, nil
}

func (m *MySQLCharacterRepository) Get(ctx context.Context, ID int) (*ent.Character, errors.CodedError) {
	var character ent.Character
	character.ID = ID
	err := m.collection().First(&character).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.NewEntityNotFound().CopyWith(errors.Message("data not found"))
		}
		return nil, errors.NewInternalSystemError().CopyWith(errors.Message("error when get data"))
	}

	return &character, nil
}
