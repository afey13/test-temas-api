package ent

type Character struct {
	ID           int    `json:"id"`
	Name         string `json:"name" gorm:"column:name"`
	HitPoints    int    `json:"hitPoints" gorm:"column:hitPoints"`
	Strength     int    `json:"strength" gorm:"column:strength"`
	Defense      int    `json:"defense" gorm:"column:defense"`
	Intelligence int    `json:"intelegence" gorm:"column:intelegence"`
	Class        int    `json:"class" gorm:"column:class"`
}
