package web

import (
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/aerilyn/service-library/log"
	"gitlab.com/afey13/test-temas-api/internal/app/character/usecase"
	"gitlab.com/afey13/test-temas-api/internal/pkg/response"
)

const (
	defaultPage  = 1
	defaultLimit = 25
)

func ListWebHandler(uc usecase.List) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		page, errResult := strconv.ParseInt(r.URL.Query().Get("page"), 10, 64)
		if errResult != nil {
			page = defaultPage
		}

		if page == 0 {
			page = 1
		}

		limit, errResult := strconv.ParseInt(r.URL.Query().Get("limit"), 10, 64)
		if errResult != nil {
			limit = defaultLimit
		}

		cmd := usecase.ListCommand{
			Page:  page,
			Limit: limit,
		}
		result, err := uc.List(ctx, cmd)
		response.Render(log.ResponseOpts{w, r, ctx, result, http.StatusOK, err})
	}
}

func AddWebHandler(uc usecase.Add) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		cmd := usecase.AddCommand{}
		if decodeErr := render.DecodeJSON(r.Body, &cmd); decodeErr != nil {
			err := errors.NewInternalSystemError().CopyWith(errors.Message(decodeErr.Error()))
			response.Render(log.ResponseOpts{w, r, ctx, nil, 0, err})
			return
		}

		result, err := uc.Add(ctx, cmd)
		response.Render(log.ResponseOpts{w, r, ctx, result, http.StatusCreated, err})
	}
}

func GetWebHandler(uc usecase.Get) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		id := chi.URLParam(r, "id")
		ID, _ := strconv.Atoi(id)
		result, err := uc.Get(ctx, ID)
		response.Render(log.ResponseOpts{w, r, ctx, result, http.StatusCreated, err})
	}
}
